/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-09-20 21:25:56
 * @LastEditors: cxguo
 * @LastEditTime: 2019-09-20 21:30:41
 */

/**
  * 样式前缀
  */
export const PREFIX = 'cx-layer-'
/**
 * 最小化时的宽度
 */
export const MIN_AREA_WIDTH = 200
/**
 * 最外层包装ID样式
 */
export const CONTAINER_CLASS_NAME = `${PREFIX}iframe-container`
/**
 * 全局选项properties的默认值
 */
export const GLOBAL_DEFAULT_PROPERTIES = {
  titleHeight: 40
}
