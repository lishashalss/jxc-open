/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-06-28 15:54:02
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-03 20:51:19
 */
import currency from 'currency.js'
const precision = 2

export const amount = (val) => {
  return currency(val, { separator: '', decimal: '.', precision, symbol: '', formatWithSymbol: true })
}

export const forEach = (arr, fn) => {
  if (!arr.length || !fn) return
  let i = -1
  const len = arr.length
  while (++i < len) {
    const item = arr[i]
    fn(item, i, arr)
  }
}

/**
 * @param {Array} arr1
 * @param {Array} arr2
 * @description 得到两个数组的交集, 两个数组的元素为数值或字符串
 */
export const getIntersection = (arr1, arr2) => {
  const len = Math.min(arr1.length, arr2.length)
  let i = -1
  const res = []
  while (++i < len) {
    const item = arr2[i]
    if (arr1.indexOf(item) > -1) res.push(item)
  }
  return res
}

/**
 * @param {Array} arr1
 * @param {Array} arr2
 * @description 得到两个数组的并集, 两个数组的元素为数值或字符串
 */
export const getUnion = (arr1, arr2) => {
  return Array.from(new Set([...arr1, ...arr2]))
}

/**
 * @param {Array} targetarr 目标数组
 * @param {Array} arr 需要查询的数组
 * @description 判断要查询的数组是否至少有一个元素包含在目标数组中
 */
export const hasOneOf = (targetarr, arr) => {
  // return targetarr.some(_ => arr.indexOf(_) > -1)
  return arr.indexOf(targetarr) > -1
}

/**
 * @param {String|Number} value 要验证的字符串或数值
 * @param {*} validList 用来验证的列表
 */
export function oneOf(value, validList) {
  for (let i = 0; i < validList.length; i++) {
    if (value === validList[i]) {
      return true
    }
  }
  return false
}

/**
 * @param {Number} timeStamp 判断时间戳格式是否是毫秒
 * @returns {Boolean}
 */
const isMillisecond = timeStamp => {
  const timeStr = String(timeStamp)
  return timeStr.length > 10
}

/**
 * @param {Number} timeStamp 传入的时间戳
 * @param {Number} currentTime 当前时间时间戳
 * @returns {Boolean} 传入的时间戳是否早于当前时间戳
 */
const isEarly = (timeStamp, currentTime) => {
  return timeStamp < currentTime
}

/**
 * @param {Number} num 数值
 * @returns {String} 处理后的字符串
 * @description 如果传入的数值小于10，即位数只有1位，则在前面补充0
 */
const getHandledValue = num => {
  return num < 10 ? '0' + num : num
}

/**
 * @param {Number} timeStamp 传入的时间戳
 * @param {Number} startType 要返回的时间字符串的格式类型，传入'year'则返回年开头的完整时间
 */
const getDate = (timeStamp, startType) => {
  const d = new Date(timeStamp * 1000)
  const year = d.getFullYear()
  const month = getHandledValue(d.getMonth() + 1)
  const date = getHandledValue(d.getDate())
  const hours = getHandledValue(d.getHours())
  const minutes = getHandledValue(d.getMinutes())
  const second = getHandledValue(d.getSeconds())
  let resStr = ''
  if (startType === 'year') resStr = year + '-' + month + '-' + date + ' ' + hours + ':' + minutes + ':' + second
  else resStr = month + '-' + date + ' ' + hours + ':' + minutes
  return resStr
}

/**
 * @param {String|Number} timeStamp 时间戳
 * @returns {String} 相对时间字符串
 */
export const getRelativeTime = timeStamp => {
  // 判断当前传入的时间戳是秒格式还是毫秒
  const IS_MILLISECOND = isMillisecond(timeStamp)
  // 如果是毫秒格式则转为秒格式
  if (IS_MILLISECOND) Math.floor(timeStamp /= 1000)
  // 传入的时间戳可以是数值或字符串类型，这里统一转为数值类型
  timeStamp = Number(timeStamp)
  // 获取当前时间时间戳
  const currentTime = Math.floor(Date.parse(new Date()) / 1000)
  // 判断传入时间戳是否早于当前时间戳
  const IS_EARLY = isEarly(timeStamp, currentTime)
  // 获取两个时间戳差值
  let diff = currentTime - timeStamp
  // 如果IS_EARLY为false则差值取反
  if (!IS_EARLY) diff = -diff
  let resStr = ''
  const dirStr = IS_EARLY ? '前' : '后'
  // 少于等于59秒
  if (diff <= 59) resStr = diff + '秒' + dirStr
  // 多于59秒，少于等于59分钟59秒
  else if (diff > 59 && diff <= 3599) resStr = Math.floor(diff / 60) + '分钟' + dirStr
  // 多于59分钟59秒，少于等于23小时59分钟59秒
  else if (diff > 3599 && diff <= 86399) resStr = Math.floor(diff / 3600) + '小时' + dirStr
  // 多于23小时59分钟59秒，少于等于29天59分钟59秒
  else if (diff > 86399 && diff <= 2623859) resStr = Math.floor(diff / 86400) + '天' + dirStr
  // 多于29天59分钟59秒，少于364天23小时59分钟59秒，且传入的时间戳早于当前
  else if (diff > 2623859 && diff <= 31567859 && IS_EARLY) resStr = getDate(timeStamp)
  else resStr = getDate(timeStamp, 'year')
  return resStr
}

/**
 * @returns {String} 当前浏览器名称
 */
export const getExplorer = () => {
  const ua = window.navigator.userAgent
  const isExplorer = (exp) => {
    return ua.indexOf(exp) > -1
  }
  if (isExplorer('MSIE')) return 'IE'
  else if (isExplorer('Firefox')) return 'Firefox'
  else if (isExplorer('Chrome')) return 'Chrome'
  else if (isExplorer('Opera')) return 'Opera'
  else if (isExplorer('Safari')) return 'Safari'
}

/**
 * @description 绑定事件 on(element, event, handler)
 */
export const on = (function() {
  if (document.addEventListener) {
    return function(element, event, handler) {
      if (element && event && handler) {
        element.addEventListener(event, handler, false)
      }
    }
  } else {
    return function(element, event, handler) {
      if (element && event && handler) {
        element.attachEvent('on' + event, handler)
      }
    }
  }
})()

/**
 * @description 解绑事件 off(element, event, handler)
 */
export const off = (function() {
  if (document.removeEventListener) {
    return function(element, event, handler) {
      if (element && event) {
        element.removeEventListener(event, handler, false)
      }
    }
  } else {
    return function(element, event, handler) {
      if (element && event) {
        element.detachEvent('on' + event, handler)
      }
    }
  }
})()

/**
 * 判断一个对象是否存在key，如果传入第二个参数key，则是判断这个obj对象是否存在key这个属性
 * 如果没有传入key这个参数，则判断obj对象是否有键值对
 */
export const hasKey = (obj, key) => {
  if (key) return key in obj
  else {
    const keysArr = Object.keys(obj)
    return keysArr.length
  }
}

/**
 * @param {*} obj1 对象
 * @param {*} obj2 对象
 * @description 判断两个对象是否相等，这两个对象的值只能是数字或字符串
 */
export const objEqual = (obj1, obj2) => {
  const keysArr1 = Object.keys(obj1)
  const keysArr2 = Object.keys(obj2)
  if (keysArr1.length !== keysArr2.length) return false
  else if (keysArr1.length === 0 && keysArr2.length === 0) return true
  /* eslint-disable-next-line */
  else return !keysArr1.some(key => obj1[key] != obj2[key])
}

/**
 * 在一个对象数组里面找到相应的对象，返回该对象的index
 * @param {*} arrObj  数组对象
 * @param {*} obj     需要找的对象
 */
export const findIndex = (arrObj, obj) => {
  const objStr = JSON.stringify(obj)
  return arrObj.reduce((total, curValue, index) => {
    if (JSON.stringify(curValue) === objStr) {
      return index
    } else {
      return total
    }
  }, -1)
}

export const emptyKeyJson = (columns) => {
  const json = {}
  columns.map((item) => {
    if (item.type === 'amount' || item.type === 'number') {
      json[item.key] = 0
    } else if (item.type === 'cascader') {
      json[item.key] = []
    } else {
      json[item.key] = ''
    }
  })
  return json
}

export const emptyKeyJsonSlot = (columns) => {
  const json = {}
  columns.map((item) => {
    if (item.type === 'select') {
      json[item.slot || item.key] = {}
    } else if (item.type === 'number') {
      json[item.slot || item.key] = 0
    } else {
      json[item.slot || item.key] = ''
    }
  })
  return json
}

/**
 * 判断是否为空
 */
export const ifNotNull = (data) => {
  if (data !== '' && data != null && typeof data !== 'undefined') {
    return true
  } else {
    return false
  }
}
/**
 * 判断是否为空(且不为0)
 */
export const ifNotNullZero = (data) => {
  if (data !== '' && data != null && data !== 0 && typeof data !== 'undefined') {
    return true
  } else {
    return false
  }
}

/**
 * 判断是否为空
 */
export const ifNotNullIncluObj = (data) => {
  if (data !== '' && data != null && typeof data !== 'undefined' && JSON.stringify(data) !== '{}') {
    if (typeof data === 'object') {
      if (ifNotNull(data.code)) {
        return true
      } else {
        return false
      }
    } else {
      return true
    }
  } else {
    return false
  }
}

/**
 * 遍历解析Json(单对象)
 * 属性置空
 */
export const reSetJson = (jsonObj) => {
  // 循环所有键
  for (var key in jsonObj) {
    var element = jsonObj[key]
    if (typeof (element) === 'string') {
      jsonObj[key] = ''
    }
    if (typeof (element) === 'number') {
      jsonObj[key] = 0
    }
    if (typeof (element) === 'undefined') {
      jsonObj[key] = undefined
    }
    if (typeof (element) === 'boolean') {
      jsonObj[key] = undefined
    }
    if (typeof (element) === 'object') {
      if (element instanceof Object) jsonObj[key] = ''
      if (element instanceof Array) jsonObj[key] = []
    }
  }
  return jsonObj
}
/**
 * 补零函数
 * @param {*} num 新数值
 * @param {*} n 旧数值
 */
const pad = (num, n) => {
  let len = num.length
  while (len < n) {
    num = '0' + num
    len++
  }
  return num
}
/**
 * 字符串尾号 顺序号+1 函数
 * @param {*} len 固定区域长度
 * @param {*} strSource 原字符串
 */
export const addSubizNo = (len, strSource) => {
  if (!len) return
  const firstStr = strSource.substr(0, len)
  const num = strSource.substring(len)
  const newNum = (Number(num) + 1).toString()
  return firstStr + pad(newNum, num.length)
}
/**
 * 判断datas是否包含id，包含就返回该对象
 * @param {*} id      需要查找的id
 * @param {*} datas   查找的对象
 */
export const hasExistLocaldata = (id, datas) => {
  if (!datas) return
  // const itemData = datas.map(item => {
  //   if (item.code === id && item.children) return item.children
  //   if (item.children) {
  //     hasExistLocaldata(id, item.children)
  //   }
  // })
  const itemData = datas.filter(item => item.code === id && item.children && item.children.length > 0)
  return itemData
}
/**
 * 根据省市的code字符串判断当前等级
 * @param {*} codeId '130200'
 */
export const levelNum = (codeId) => {
  if (!codeId) return 0
  const scendStr = codeId.substr(2, 2)
  const lastStr = codeId.substr(4, 2)
  if (scendStr === '00') {
    return 1
  } else if (lastStr === '00') {
    return 2
  } else {
    return 3
  }
}

/**
 * 迭代寻找“能获取”焦点的控件
 * @param {*} elNodeList 元素list
 * @param {*} index 索引
 */
export const findValidEL = (elNodeList, index = 0) => {
  // 没有下一个控件，直接返回
  if (!elNodeList.item(index)) return undefined

  // 如控件已被禁用，则迭代寻找“有效”控件
  if (elNodeList.item(index).disabled && !elNodeList.item(index).readOnly) {
    return findValidEL(elNodeList, index + 1)
  }

  return elNodeList.item(index)
}

/**
 * 乘法函数，用来得到精确的乘法结果
 * 说明：javascript的乘法结果会有误差，在两个浮点数相乘的时候会比较明显。这个函数返回较为精确的乘法结果。
 * 调用：accMul(arg1,arg2)
 * 返回值：arg1乘以arg2的精确结果
 */
export const accMul = (arg1, arg2) => {
  var m = 0; var s1 = arg1.toString()
  var s2 = arg2.toString()
  try {
    m += s1.split('.')[1].length
  // eslint-disable-next-line no-empty
  } catch (e) {}
  try {
    m += s2.split('.')[1].length
  // eslint-disable-next-line no-empty
  } catch (e) {}
  return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10, m)
}

/**
 * 除法函数，用来得到精确的除法结果
 * 说明：javascript的乘法结果会有误差，在两个浮点数相乘的时候会比较明显。这个函数返回较为精确的乘法结果。
 * 调用：accMul(arg1,arg2)
 * 返回值：arg1乘以arg2的精确结果
 */
export const accDiv = (arg1, arg2) => {
  var t1 = 0; var t2 = 0; var r1; var r2
  try {
    t1 = arg1.toString().split('.')[1].length
  // eslint-disable-next-line no-empty
  } catch (e) { }

  try {
    t2 = arg2.toString().split('.')[1].length
  // eslint-disable-next-line no-empty
  } catch (e) { }

  r1 = Number(arg1.toString().replace('.', ''))
  r2 = Number(arg2.toString().replace('.', ''))
  return (r1 / r2) * Math.pow(10, t2 - t1)
}
/**
 * 加法函数，用来得到精确的加法结果
 * 说明：javascript的加法结果会有误差，在两个浮点数相加的时候会比较明显。这个函数返回较为精确的加法结果。。
 * 调用：accAdd(arg1,arg2)
 * 返回值：arg1加上arg2的精确结果
 */
export const accAdd = (arg1, arg2) => {
  var r1, r2, m
  try {
    r1 = arg1.toString().split('.')[1].length
  } catch (e) {
    r1 = 0
  } try {
    r2 = arg2.toString().split('.')[1].length
  } catch (e) { r2 = 0 } m = Math.pow(10, Math.max(r1, r2))
  return (arg1 * m + arg2 * m) / m
}

/**
 * 迭代寻找“能获取”焦点的控件
 * @param {*} pickObj 元素需要pick的对象
 * @param {*} json 对象
 * return 包含所有keys的新对象
 */
export const pick = (pickObj, json) => {
  const temp = {}
  const objKeys = Object.keys(pickObj)
  const JsonKeys = Object.keys(json)
  JsonKeys.forEach(item => {
    if (objKeys.indexOf(item) > -1) {
      temp[item] = pickObj[item]
    } else {
      temp[item] = json[item]
    }
  })
  return temp
}

/**
 * json数组根据某一字段去重后的
 * param1:json数组，param2:json数组
 * @param [{}...]  需要去重的json数组
 * @param '' 某一字段 string
 * return 去重后的key数组
 */
export const jsonArrPickByKey = (arr, key) => {
  const temp = []
  arr.forEach(item => {
    temp.push(item[key])
  })
  return Array.from(new Set(temp))
}

/**
 * 遍历带children的json数组，根据传入条件 与 [str] 字段比较 --》添加字段checked: true/false
 */
export const ergodicJsonArrChild = (jsonArr, strArr) => {
  jsonArr.map(item => {
    if (strArr.indexOf(item.value) > -1) item.checked = true
    else item.checked = false
    if (item.children) ergodicJsonArrChild(item.children, strArr)
  })
}

/**
 * 获取uuid
 */
export const uuid = () => {
  var s = []
  var hexDigits = '0123456789abcdef'
  for (var i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)
  }
  s[14] = '4' // bits 12-15 of the time_hi_and_version field to 0010
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1) // bits 6-7 of the clock_seq_hi_and_reserved to 01
  s[8] = s[13] = s[18] = s[23] = '-'

  var uuid = s.join('')
  return uuid
}

// 时间格式化
export const dateFormat = (date, fmt) => {
  if (typeof (date) === 'string') {
    date = new Date(date)
  }
  var o = {
    'M+': date.getMonth() + 1, // 月份
    'd+': date.getDate(), // 日
    'h+': date.getHours(), // 小时
    'm+': date.getMinutes(), // 分
    's+': date.getSeconds(), // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    'S': date.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (var k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}

/**
 * 增值税税额计算（金额*税率/(1+税率）
 * amount:金额
 * taxRate:税率
 * return 增值税税额
 */
export const vatCount = (amount, taxRate) => {
  return accDiv(accMul(amount, taxRate / 100), accAdd(1, taxRate / 100))
}

/**
 * 利润 = 应收*汇率 - 应收税额 - （应付*汇率 - 应付税额）
 * income:应收
 * cost:应付
 * costTax:应付税额
 * incomeTax:应收税额
 */
export const countCostAndIncomeTable = (costList, incomelist) => {
  let income = 0
  let cost = 0
  let incomeTax = 0
  let costTax = 0
  let profit = 0
  if (costList.length > 0) {
    costList.forEach(item => {
      if (ifNotNullZero(item.taxRate)) {
        if (ifNotNullZero(item.currencyRate)) {
          costTax = accAdd(costTax, vatCount(accMul(item.amount, item.currencyRate), item.taxRate))
        } else {
          costTax = accAdd(costTax, vatCount(item.amount, item.taxRate))
        }
      }
      if (ifNotNullZero(item.currencyRate)) {
        cost = accAdd(cost, accMul(item.amount, item.currencyRate))
      } else {
        cost = accAdd(cost, item.amount)
      }
    })
  }
  if (incomelist.length > 0) {
    incomelist.forEach(item => {
      if (ifNotNullZero(item.taxRate)) {
        if (ifNotNullZero(item.currencyRate)) {
          incomeTax = accAdd(incomeTax, vatCount(accMul(item.amount, item.currencyRate), item.taxRate))
        } else {
          incomeTax = accAdd(incomeTax, vatCount(item.amount, item.taxRate))
        }
      }
      if (ifNotNullZero(item.currencyRate)) {
        income = accAdd(income, accMul(item.amount, item.currencyRate))
      } else {
        income = accAdd(income, item.amount)
      }
    })
  }
  // 计算利润
  profit = accAdd(accAdd(income, -incomeTax), -accAdd(cost, -costTax))
  return { 'income': income, 'cost': cost, 'profit': profit }
}

// 数字格式化添加千分位'，'
export const numToThousands = (num) => {
  var str = num.toFixed(2).toString()
  var reg = str.indexOf('.') > -1 ? /(\d)(?=(\d{3})+\.)/g : /(\d)(?=(?:\d{3})+$)/g
  return str.replace(reg, '$1,')
}

// dateRangeValidate
export const dateRangeValidate = (rule, value, callback) => {
  if (value && value.length > 0) {
    if (!(value[0] instanceof Date) || !(value[1] instanceof Date)) {
      callback(new Error('请选择时间范围'))
    }
  }
  callback()
}

// 初始化指定字段在对象中的值
export const initKeysInObj = (jsonObj, arrKeys) => {
  const temp = []
  arrKeys.map(key => {
    temp.push(jsonObj.map(item => {
      if (item[key]) {
        if (typeof (item[key]) === 'string') {
          item[key] = ''
        }
        if (typeof (item[key]) === 'number') {
          item[key] = 0
        }
        if (typeof (item[key]) === 'boolean') {
          item[key] = undefined
        }
        if (typeof (item[key]) === 'object') {
          item[key].code = ''
          item[key].name = ''
        }
      }
    }))
  })
  return jsonObj
}

// 驼峰转换下划线
export const hump2Line = (name) => {
  return name.replace(/([A-Z])/g, '_$1').toLowerCase()
}
