/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-03-28 14:09:53
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-11 19:56:38
 */
import Vue from 'vue'
import store from '@/store'

/**
 * 常用的data和方法
 */
export default {
  data() {
    return {
      // appData
      appData: store.getters.appData,
      currentUserId: store.getters.appData.userInfo.userId,
      //
      btnLoading: {},
      tableLoading: {},
      dataObj: {},
      // 多个表格 - 一般table
      tableData: {},
      tableSelectionData: {},
      pageInfo: {
        current: 1, // 当前页
        size: 20, // 每页数据条数
        sort: '', // 排序字段
        order: 'desc', // 排序方向
        total: 0 // 总数居量
      },
      searchData: {},
      isContinue: false,
      // options
      optionsYesNo: [
        { code: 1, name: '是' },
        { code: 0, name: '否' }
      ]
    }
  },
  computed: {
  },
  methods: {
    // ### UI ###
    btnResetForm(name) {
      this.resetForm(name)
    },
    // ### API ###
    setBtnLoading(name, status) {
      Vue.set(this.btnLoading, name, status)
    },
    setTableLoading(name, status) {
      Vue.set(this.tableLoading, name, status)
    },
    setTableData(name, data) {
      Vue.set(this.tableData, name, data)
    },
    getTableData(name) {
      return this.tableData[name] || []
    },
    setTableSelectionData(name, data) {
      Vue.set(this.tableSelectionData, name, data)
    },
    getTableSelectionData(name, message = true) {
      if (!this.tableSelectionData[name] || this.tableSelectionData[name].length === 0) {
        message && this.$message.error('请至少选择一条数据!')
        return false
      }
      return this.tableSelectionData[name]
    },
    handleValid(name) {
      return new Promise((resolve, reject) => {
        this.$refs[name].validate((valid) => {
          if (!valid) {
            this.$message.error('表单验证失败!')
          } else {
            resolve()
          }
        })
      })
    },
    /**
     * 重置表单
     * @param {表单ref名} name
     */
    resetForm(name) {
      if (this.$refs[name]) this.$refs[name].resetFields()
    },
    /**
     * 是否删除
     */
    isAllowDel(message) {
      return this.$confirm(message || '确定删除吗?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
    },
    closeCurrentTag(view) {
      this.$store.dispatch('tagsView/delView', view)
    }
  }
}
