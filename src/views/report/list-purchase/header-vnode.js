/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-05-22 13:28:56
 * @LastEditors: cxguo
 * @LastEditTime: 2020-05-24 12:15:57
 */

/**
 * 获取销售毛利比重
 */
export const getGrossProfitProportion = function() {
  // eslint-disable-next-line no-unused-vars
  const h = this.$createElement
  return [
    <el-tooltip effect='dark' content='毛利比重 = 该商品销售毛利 / 所有商品销售毛利之和，的百分比' placement='top-end'>
      <span>毛利比重<i class='color-bule el-icon-question'/></span>
    </el-tooltip>
  ]
}

/**
 * 获取销售毛利 header
 */
export const getGrossProfit = function() {
  // eslint-disable-next-line no-unused-vars
  const h = this.$createElement
  return [
    <el-tooltip effect='dark' content='销售毛利 = 销售额 - （ 销售量 * 成本价 ）' placement='top-end'>
      <span>销售毛利<i class='color-bule el-icon-question'/></span>
    </el-tooltip>
  ]
}

