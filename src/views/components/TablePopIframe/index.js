/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-03-05 21:59:14
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-05 22:46:58
 */

import Vue from 'vue'

// 字符串解析成dom
function parseDom(arg) {
  var objE = document.createElement('span')
  objE.innerHTML = arg
  return objE.childNodes
}
export default function(container, content, props) {
  const box = parseDom('<div id="table-layer"></div>')[0]
  container.appendChild(box)
  const Cls = Vue.extend(content)
  const instance = Object.assign(new Cls(), props)
  const iframe = instance.$mount()
  box.appendChild(iframe.$el)
  return iframe
}
