/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-02-02 10:06:15
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-12 17:34:37
 */
import { getAddButton, getRefreshButton, getUpdateButton, getDelButton } from '@/components/Table/oper-btns.js'

export default {
  name: 'oper-buttons',
  render(h, context) {
    const _this = this
    return h('div', {
      'class': {
        'tree-cat': true
      }
    }, [
      getAddButton(h, { on: { click: () => { _this.$emit('on-add') } }}),
      getUpdateButton(h, { on: { click: () => { _this.$emit('on-update') } }}),
      getDelButton(h, { on: { click: () => { _this.$emit('on-del') } }}),
      getRefreshButton(h, { on: { click: () => { _this.$emit('on-refresh') } }})
    ])
  }
}
