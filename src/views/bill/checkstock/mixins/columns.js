/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-10-18 15:37:24
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-26 15:35:56
 */
export default {
  data() {
    return {
      columnsAllData: [
        { field: 'code', title: '商品编号', width: 150 },
        { field: 'name', title: '商品名称', width: 150 },
        { field: 'bookQty', title: '账面数量', width: 150 },
        { field: 'realQty', title: '实际数量', width: 150 },
        { field: 'disQty', title: '盘亏数量', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { bookQty, realQty } = row
              const disStr = this.$amount(realQty).subtract(bookQty).format()
              const dis = Number(disStr)
              const value = dis > 0 ? '' : disStr
              return [<span>{value}</span>]
            }
          }
        },
        { field: 'disAmount', title: '盘亏金额', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { bookQty, realQty } = row
              const disStr = this.$amount(realQty).subtract(bookQty).format()
              const dis = Number(disStr)
              const value = dis > 0 ? '' : disStr
              return [<span>{value}</span>]
            },
            header: () => {
              return [
                <el-tooltip effect='dark' content='盘亏金额 = 盘亏数量 * 成本价' placement='top-end'>
                  <span>盘亏金额<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        },
        { field: 'disQty', title: '盘盈数量', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { bookQty, realQty } = row
              const disStr = this.$amount(bookQty).subtract(realQty).format()
              const dis = Number(disStr)
              const value = dis > 0 ? '' : disStr
              return [<span>{value}</span>]
            }
          }
        },
        { field: 'disAmount', title: '盘盈金额', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { bookQty, realQty } = row
              const disStr = this.$amount(bookQty).subtract(realQty).format()
              const dis = Number(disStr)
              const value = dis > 0 ? '' : disStr
              return [<span>{value}</span>]
            },
            header: () => {
              return [
                <el-tooltip effect='dark' content='盘盈金额 = 盘盈数量 * 成本价' placement='top-end'>
                  <span>盘盈金额<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        }
      ],
      columnsLossData: [
        { field: 'code', title: '商品编号', width: 150 },
        { field: 'name', title: '商品名称', width: 150 },
        { field: 'bookQty', title: '账面数量', width: 150 },
        { field: 'realQty', title: '实际数量', width: 150 },
        { field: 'disQty', title: '盘亏数量', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { bookQty, realQty } = row
              const disStr = this.$amount(realQty).subtract(bookQty).format()
              const dis = Number(disStr)
              const value = dis > 0 ? '' : disStr
              return [<span>{value}</span>]
            }
          }
        },
        { field: 'disAmount', title: '盘亏金额', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { bookQty, realQty } = row
              const disStr = this.$amount(realQty).subtract(bookQty).format()
              const dis = Number(disStr)
              const value = dis > 0 ? '' : disStr
              return [<span>{value}</span>]
            },
            header: () => {
              return [
                <el-tooltip effect='dark' content='盘亏金额 = 盘亏数量 * 成本价' placement='top-end'>
                  <span>盘亏金额<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        }
      ],
      columnsProfitData: [
        { field: 'code', title: '商品编号', width: 150 },
        { field: 'name', title: '商品名称', width: 150 },
        { field: 'bookQty', title: '账面数量', width: 150 },
        { field: 'realQty', title: '实际数量', width: 150 },
        { field: 'disQty', title: '盘盈数量', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { bookQty, realQty } = row
              const disStr = this.$amount(bookQty).subtract(realQty).format()
              const dis = Number(disStr)
              const value = dis > 0 ? '' : disStr
              return [<span>{value}</span>]
            }
          }
        },
        { field: 'disAmount', title: '盘盈金额', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { bookQty, realQty } = row
              const disStr = this.$amount(bookQty).subtract(realQty).format()
              const dis = Number(disStr)
              const value = dis > 0 ? '' : disStr
              return [<span>{value}</span>]
            },
            header: () => {
              return [
                <el-tooltip effect='dark' content='盘盈金额 = 盘盈数量 * 成本价' placement='top-end'>
                  <span>盘盈金额<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        }
      ]
    }
  },
  methods: {
  }
}
