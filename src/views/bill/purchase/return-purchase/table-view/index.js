/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-16 22:20:19
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-09 09:56:22
 */
import GoodsSelect from '@/views/goods-select'

export default {
  extends: GoodsSelect,
  mixins: [GoodsSelect],
  props: {
  },
  data() {
    return {}
  },
  created() {
    const originColumnsData = this.originColumnsData
    const nameCol = originColumnsData.find(item => { return item.field === 'name' })
    const priceCol = originColumnsData.find(item => { return item.field === 'price' })
    const quantityCol = originColumnsData.find(item => { return item.field === 'quantity' })
    const unitNameCol = originColumnsData.find(item => { return item.field === 'unitName' })
    this.$delete(nameCol, 'editRender')
    this.$delete(quantityCol, 'editRender')
    this.$delete(priceCol, 'editRender')
    this.$delete(unitNameCol, 'editRender')
    this.originColumnsData = originColumnsData
  },
  watch: {
  },
  methods: {
  }
}
