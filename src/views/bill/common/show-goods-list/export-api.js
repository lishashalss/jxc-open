/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-05-10 18:21:40
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-06 14:03:22
 */
export default {
  data() {
    return {}
  },
  methods: {
    setTableLoading(val) {
      this.loading = val
    },
    async valiData() {
      const postData = this.getPostData()
      return this.$refs.table.validate(postData).catch(errMap => errMap)
    }
  }
}
