import utils from '../utils'

const DATA = {
  isDragging: false,
  minColumnWidth: 10,
  draggingStartX: 0,
  draggingEndX: 0
}
const getHeaderCols = (table) => {
  return table.querySelectorAll('.ivu-table-header table colgroup col')
}

const getBodyCols = (table) => {
  return table.querySelectorAll('.ivu-table-body table colgroup col')
}

const getDrags = (table) => {
  return table.querySelectorAll('.ivu-table-header thead th')
}

const getDomHeight = (obj) => {
  return obj.offsetHeight
}

const getDomWidth = (obj) => {
  return obj.offsetWidth
}

/**
 * 获取点击列的整体宽高
 * @param {点击对象th} that
 * @param {表对象} table
 */
const getDragsWH = (that, table) => {
  const index = that.getAttribute('index')
  let result = {
    w: getDomWidth(that),
    h: getDomHeight(that)
  }
  const tableRows = table.querySelectorAll('.ivu-table-body .ivu-table-row')
  tableRows.forEach(row => {
    const ths = row.children
    result.h += getDomHeight(ths[index])
  })
  return result
}

const getAbsPos = (obj) => {
  var pos = {}
  pos.x = obj.offsetLeft
  pos.y = obj.offsetTop
  // eslint-disable-next-line no-cond-assign
  while (obj = obj.offsetParent) {
    pos.x += obj.offsetLeft
    pos.y += obj.offsetTop
  }
  return pos
}

const getDragTarget = (absPos, area) => {
  const dragTarget = document.createElement('div')
  dragTarget.id = 'tableDragLine'
  dragTarget.style.borderRight = '#cccccc 1px dashed'
  dragTarget.style.width = area.w + 'px'
  dragTarget.style.height = area.h + 'px'
  dragTarget.style.position = 'absolute'
  dragTarget.style.left = absPos.x + 'px'
  dragTarget.style.top = absPos.y + 'px'
  return dragTarget
}

const removeDragTarget = () => {
  const dragTarget = document.getElementById('tableDragLine')
  if (dragTarget) dragTarget.remove()
}

const handleMouseUp = () => {
  // console.log('handleMouseUp')
  removeDragTarget()
  DATA.isDragging = false
  utils.unbind(document.body, 'mousemove', handleMouseMove)
  utils.unbind(document.body, 'mouseup', handleMouseUp)
}

const handleMouseMove = (e) => {
  if (!DATA.isDragging) return false

  // 更改鼠标样式
  if (e.offsetX > this.offsetWidth - DATA.minColumnWidth) this.style.cursor = 'col-resize'
  else this.style.cursor = 'default'

  if (DATA.draggingStartX === 0) {
    DATA.draggingStartX = e.clientX
  }
  DATA.draggingEndX = e.clientX
  let disX = DATA.draggingEndX - DATA.draggingStartX
  // console.log('disX:', disX)
  DATA.draggingStartX = e.clientX
  // console.log('---------------')
}

const handleMouseDown = (that, table) => {
  // console.log('绝对位置:', getAbsPos(that))
  // console.log('拖拽层宽高:', getDragsWH(that, table))
  const dragTarget = getDragTarget(getAbsPos(that), getDragsWH(that, table))
  window.dragTarget = dragTarget
  document.body.appendChild(dragTarget)
  DATA.isDragging = true
  utils.bind(document.body, 'mousemove', handleMouseMove)
  utils.bind(document.body, 'mouseup', handleMouseUp)
}

const resize = (that, table, targetWidth) => {
  const index = that.getAttribute('index')
  const headerCols = getHeaderCols(table)
  const bodyCols = getBodyCols(table)
  headerCols[index].width = targetWidth
  bodyCols[index].width = targetWidth
}

export default {
  inserted: (el, binding, vnode) => {
    const table = el
    const ths = getDrags(table)
    for (var j = 0; j < ths.length; j++) {
      ths[j].setAttribute('index', j) // 初始化索引

      ths[j].onmousemove = function () {

      }

      ths[j].onmousedown = function () {
        if (event.offsetX > this.offsetWidth - DATA.minColumnWidth) handleMouseDown(this, table)
      }
    }
  },
  update: (el, binding, vnode) => {
  }
}
