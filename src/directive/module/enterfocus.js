import { findValidEL } from '@/libs/tools'
export default {
  bind: (el, binding) => {
    const selectors = 'input:enabled:read-write,textarea:enabled:read-write,[type=checkbox]:enabled,[type=radio]:enabled'
    const eleNodeList = el.querySelectorAll(selectors)
    // 首个输入框默认获取焦点
    setTimeout(() => {
      const els = Array.from(eleNodeList).find(els => els.type !== 'button' && !els.disabled && !els.readOnly)
      els && els.focus()
    }, 300)
    // console.log('01010101010100', eleNodeList)
    // 为每个数据框增加监听事件，回车获取焦点
    eleNodeList.forEach((ele, index) => {
      ele.addEventListener('keyup', ev => {
        if (ev.keyCode === 13) {
          // 是文本域，执行内部换行动作，不继续回车获取焦点逻辑 eleNodeList.item(index).type === ‘textarea’
          if (eleNodeList.item(index).getAttribute('rows')) return
          const elTemp = findValidEL(eleNodeList, index + 1)
          elTemp && elTemp.focus()
        }
      })
    })
  }
}
