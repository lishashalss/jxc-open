/*
 * @Descripttion: 财务往来账
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-15 20:08:43
 */
import axios from '@/utils/request'
const baseUrl = '/fin_comego'

export function addPaymentData(params) {
  return axios.request({
    url: `${baseUrl}/add_payment_data`,
    method: 'post',
    data: params
  })
}

export function addReceiptData(params) {
  return axios.request({
    url: `${baseUrl}/add_receipt_data`,
    method: 'post',
    data: params
  })
}

export function listReceiptData(params) {
  return axios.request({
    url: `${baseUrl}/list_receipt_data`,
    method: 'post',
    data: params
  })
}

export function listPaymentData(params) {
  return axios.request({
    url: `${baseUrl}/list_payment_data`,
    method: 'post',
    data: params
  })
}

export function getPaymentDetailDataByMaster(params) {
  return axios.request({
    url: `${baseUrl}/get_payment_detail_bymaster`,
    method: 'post',
    data: params
  })
}
export function getReceiptDetailDataByMaster(params) {
  return axios.request({
    url: `${baseUrl}/get_receipt_detail_bymaster`,
    method: 'post',
    data: params
  })
}

