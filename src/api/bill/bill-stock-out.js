/*
 * @Descripttion: 进出库票据接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 14:38:17
 */
import axios from '@/utils/request'
const baseUrl = '/bill/stockOut'

/**
 * 新增出库单
 * @param {*} params
 */
export function addStockOut(params) {
  return axios.request({
    url: `${baseUrl}/addData`,
    method: 'post',
    data: params
  })
}

export function cancleBill(params) {
  return axios.request({
    url: `${baseUrl}/cancle`,
    method: 'post',
    data: params
  })
}

/**
 * 查询待出库单子
 * @param {*} params
 */
export function getWaiteStockOutBillList(params) {
  return axios.request({
    url: `${baseUrl}/listWaite2OutBillsPage`,
    method: 'post',
    data: params
  })
}

/**
 * 查询已出库单子
 * @param {*} params
 */
export function getHasStockOutBillList(params) {
  return axios.request({
    url: `${baseUrl}/listHasOutBillsPage`,
    method: 'post',
    data: params
  })
}

export function getWaiteDetail(billId) {
  return axios.request({
    url: `${baseUrl}/getBillAndDetailsOfWaite/${billId}`,
    method: 'post',
    data: billId
  })
}

export function getBillcode(params) {
  return axios.request({
    url: `${baseUrl}/getBillNo`,
    method: 'post',
    data: params
  })
}

export function getBillDetail(billId) {
  return axios.request({
    url: `${baseUrl}/getBillAndDetails/${billId}`,
    method: 'post',
    data: billId
  })
}

export function print(params) {
  return axios.request({
    url: `${baseUrl}/print`,
    method: 'post',
    data: params
  })
}
