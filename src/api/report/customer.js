/*
 * @Descripttion: 客户接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2019-08-20 17:59:40
 */
import axios from '@/utils/request'
const baseUrl = '/admin/customer'

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/save`,
    method: 'post',
    data: params
  })
}

export function delData(params) {
  return axios.request({
    url: `${baseUrl}/delete`,
    method: 'post',
    data: params
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/list`,
    method: 'post',
    data: params
  })
}

export function getdataById(params) {
  return axios.request({
    url: `${baseUrl}/getdataById`,
    method: 'post',
    data: params
  })
}

